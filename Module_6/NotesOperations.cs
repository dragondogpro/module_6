﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_6
{
    class NotesOperations
    {

        public void PrintNotes(List<Note> notes)
        {
            foreach (Note note in notes)
                Console.WriteLine(note.PrintNote());
        }

        public List<Note> DeleteNote(List<Note> notes, int id)
        {
            bool isGood = false;

            for (int i = 0; i < notes.Count; i++)
            {
                if (notes[i].Id == id)
                {
                    notes.RemoveAt(i);
                    isGood = true;
                    break;
                }  
            }

            if (isGood)
                Console.WriteLine($"Запись под номером {id} удалена");
            else
                Console.WriteLine($"Записи под номером {id} не найдено");

            return notes;
        }


        public Note EditNote(Note note, string title, string text, DateTime dateComplited, bool isComplited)
        {
            note.Title = title;
            note.Text = text;
            note.DateComplited = dateComplited;
            note.IsComplited = true;

            return note;
        }

        public List<Note> AppendNotes(List<Note> notes, string pathToFile)
        {
            FileHelper fileHelper = new FileHelper();

            List<Note> newNotesList = fileHelper.CSVFileRead(pathToFile);

            foreach (Note note in newNotesList)
            {
                notes.Add(new Note(notes.Count, note.Title, note.Text, 
                    note.DateCreated, note.DateComplited, note.IsComplited));
            }

            return notes;
        }

        public List<Note> SortNotesOn(List<Note> noteList, string field)
        {
            List<Note> sortedNote = new List<Note>(noteList);

            switch (field)
            {
                case "Id":
                    sortedNote.Sort((emp1, emp2) => emp1.Id.CompareTo(emp2.Id));
                    break;

                case "Title":
                    sortedNote.Sort((emp1, emp2) => emp1.Title.CompareTo(emp2.Title));
                    break;

                case "Text":
                    sortedNote.Sort((emp1, emp2) => emp1.Text.CompareTo(emp2.Text));
                    break;

                case "DateStart":
                    sortedNote.Sort((emp1, emp2) => emp1.DateCreated.CompareTo(emp2.DateCreated));
                    break;

                case "DateEnd":
                    sortedNote.Sort((emp1, emp2) => emp1.DateComplited.CompareTo(emp2.DateComplited));
                    break;

                case "IsComplited":
                    sortedNote.Sort((emp1, emp2) => emp1.IsComplited.CompareTo(emp2.IsComplited));
                    break;

                default:
                    Console.WriteLine("Поле не найдено");
                    break;
            }

            return sortedNote;
        }

        public List<Note> SortOnRangeDate(List<Note> noteList, DateTime startRange, DateTime endRange)
        {
            List<Note> sortedList = new List<Note>();

            foreach (Note note in noteList)
            {
                if ((note.DateCreated >= startRange) && (note.DateCreated <= endRange))
                {
                    sortedList.Add(note);
                }
            }           
            
            return sortedList;
        }
    }
}
