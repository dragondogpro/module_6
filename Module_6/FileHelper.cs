﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_6
{
    class FileHelper
    {
        public List<Note> CSVFileRead(string pathToFile)
        {
            using (StreamReader streamReader = new StreamReader(pathToFile))
            {
                using (CsvReader csvReader = new CsvReader(streamReader, CultureInfo.InvariantCulture))
                {

                    csvReader.Configuration.Delimiter = ";";

                    var notes = csvReader.GetRecords<Note>();
                    return notes.ToList();
                }
            }
        }

        public void CSVFileWrite(string pathToFile, List<Note> noteList)
        {
            using (StreamWriter streamReader = new StreamWriter(pathToFile, false))
            {
                using (CsvWriter csvReader = new CsvWriter(streamReader, CultureInfo.InvariantCulture))
                {
                    csvReader.Configuration.Delimiter = ";";
                    csvReader.WriteRecords(noteList);
                }
            }
        }
    }
}
