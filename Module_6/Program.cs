﻿using CsvHelper;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Module_6
{
    class Program
    {
        static void Main(string[] args)
        {
            FileHelper helper = new FileHelper();
            NotesOperations notesOperations = new NotesOperations();
            string pathToFile = "base.csv";

            //Получение всех записей из файла
            List<Note> notesList = helper.CSVFileRead(pathToFile);

            //Добавление записей 
            notesList.Add(new Note(notesList.Count(), "title tets", "text test"));

            //Удаление записей по айдишнику
            notesOperations.DeleteNote(notesList, 2);

            //Редактирование записей
            notesOperations.EditNote(notesList[notesList.Count - 1], "title edited", "text edited",
                notesList[notesList.Count - 1].DateComplited, notesList[notesList.Count - 1].IsComplited);

            //иморт в файл из другого файла
            notesOperations.AppendNotes(notesList, "baseAppend.csv");

            //Печать всех записей
            notesOperations.PrintNotes(notesList);

            Console.WriteLine("-----------------------------------------------------");

            //Сортировка записей по полю
            notesOperations.PrintNotes(notesOperations.SortNotesOn(notesList, "IsComplited"));

            Console.WriteLine("-----------------------------------------------------");

            notesOperations.PrintNotes(notesOperations.SortOnRangeDate(notesList,
                new DateTime(2020, 01, 01), new DateTime(2020, 02, 01)));

            //Сохранение в файл
            helper.CSVFileWrite(pathToFile, notesList);
        }
    }
}
