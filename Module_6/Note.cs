﻿using System;
using CsvHelper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration.Attributes;

namespace Module_6
{
    class Note
    {
        [Name("id")]
        public int Id { get; set; }

        [Name("title")]
        public string Title { get; set; }

        [Name("text")]
        public string Text { get; set; }

        [Name("dateCreated")]
        public DateTime DateCreated { get; set; }

        [Name("dateComplited")]
        public DateTime DateComplited { get; set; }

        [Name("isComplited")]
        public bool IsComplited { get; set; }


        public Note(int id, string title, string text, DateTime dateCreated, DateTime dateComplited, bool isComplited)
        {
            Id = id;
            Title = title;
            Text = text;
            DateCreated = dateCreated;
            DateComplited = dateComplited;
            IsComplited = isComplited;
        }

        public Note(int id, string title, string text, DateTime dateCreated, DateTime dateComplited) :
            this(id, title, text, dateCreated, dateComplited, false)
        { }

        public Note(int id, string title, string text, DateTime dateComplited) :
            this(id, title, text, DateTime.Today, dateComplited, false)
        {  }

        public Note(int id, string title, string text) :
            this(id, title, text, DateTime.Today, DateTime.Today, false)
        { }

        public Note(int id, string title) :
            this(id, title, String.Empty, DateTime.Today, DateTime.Today, false)
        { }

        public string PrintNote()
        {
            string status = IsComplited ? "В процессе" : "Выполнена";
            return $"№ {Id}\n" +
                   $"Заголовок: {Title}\n" +
                   $"Текст: {Text}\n" +
                   $"Дата создания: {DateCreated.ToShortDateString()}\n" +
                   $"Дата завершения: {DateComplited.ToShortDateString()}\n" +
                   $"Статус выполнения: {status}\n";
        }
    }
}
